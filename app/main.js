import React from 'react';
import AppContainer from '../ui/AppContainer.jsx';
import ApolloClient, { createNetworkInterface } from 'apollo-client';
import { render } from 'react-dom';
import { ApolloProvider } from 'react-apollo';
import { registerGqlTag } from 'apollo-client/gql';
import polyfill from 'es6-promise';
polyfill.polyfill();
import 'isomorphic-fetch';
require('!style!css!less!./main.less');

registerGqlTag();

const networkInterface = createNetworkInterface('http://159.100.249.141:4001/graphql');
const client = new ApolloClient({ networkInterface });

render((
  <ApolloProvider client={client}>
    <AppContainer />
  </ApolloProvider>
), document.getElementById('app'));
