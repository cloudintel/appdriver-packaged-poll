import { connect } from 'react-apollo';
import App from './App.jsx';

const getParameterByName = function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
};

function mapQueriesToProps({ ownProps, state }) {
  return {
    vatom: {
      query: gql`query getVatom($id: ID!, $loginToken: String!) {
        oneVatom(id: $id, loginToken: $loginToken) {
          id
          is_syncing
          private {
            questions {
                first {
                answers_aggregated {
                    true
                     false
                }
                 question
              }
              }
          }
        }
      }`,
      variables: {
        id: getParameterByName('vatomId') || '196edeab-4149-41e3-b514-b0f420346474',
        loginToken: getParameterByName('userToken') || 'YBFu0JXsGteGzD7rOYs8I4R2l7fK1d67KLa-gK0hDGD',
      },
      forceFetch: true,
      returnPartialData: false,
    },
  };
}

function mapMutationsToProps({ ownProps, state }) {
  return {
    postReply: (id, name, answers) => ({
      mutation: gql`mutation anwerPoll($id: ID!, $name: String!, $answers: PollAnswers!) {
    updatePoll(
        id: $id
        name: $name
        answers: $answers
    ) {
        id
        is_syncing
        private {
     questions {
       first {
         answers_aggregated {
           true
           false
         }
         question
       }
     }
        }
    }
}`,
      variables: {
        id,
        name,
        answers,
      },
    }),
  };
}

const AppContainer = connect({
  mapQueriesToProps,
  mapMutationsToProps,
})(App);

export default AppContainer;
