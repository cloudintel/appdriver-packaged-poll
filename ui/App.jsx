import React, { Component } from 'react';
import 'react-dom';
import { Pie } from 'react-chartjs';
import CountryFlags from './CountryFlags';
import { countries } from 'country-data';
import { country_reverse_geocoding } from 'country-reverse-geocoding';

export default class App extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      expandedSector: null,
    };
  }

  componentDidMount() {
    this.props.vatom.startPolling(200);
    navigator.geolocation.getCurrentPosition((position) => {
      const country = country_reverse_geocoding().get_country(
        position.coords.latitude,
        position.coords.longitude);
      this.setState({ flag: countries[country.code].alpha2 });
    }, (error) => {
      console.log(error);
    }, { maximumAge: 600000 });
  }

  handleMouseEnterOnSector(sector) {
    this.setState({ expandedSector: sector });
  }

  handleMouseLeaveFromSector() {
    this.setState({ expandedSector: null });
  }

  yes() {
    this.props.mutations.postReply(this.props.vatom.oneVatom.id, 'Submit', {
      first: true,
    });
  }

  no() {
    this.props.mutations.postReply(this.props.vatom.oneVatom.id, 'Submit', {
      first: false,
    });
  }

  render() {
    if (this.props.vatom.loading) {
      return (
        <div>
          <div>
            Loading...
          </div>
        </div>
      );
    }
    const vatomOne = this.props.vatom.oneVatom.private.questions.first;
    const yesValue = vatomOne.answers_aggregated.true;
    const noValue = vatomOne.answers_aggregated.false;
    const question = vatomOne.question;

    const data = [
      { label: 'Yes', value: yesValue, color: '#00ff00' },
      { label: 'No', value: noValue, color: '#ff0000' },
    ];
    const options = {};
    const flag = CountryFlags[this.state.flag];
    return (
      <div>
        <div>
          <p>{ question }</p>
          <button onClick={this.yes.bind(this)}>Yes</button>
          <button onClick={this.no.bind(this)}>No</button>
        </div>
        <img src={flag}/>
        <Pie
          data={data}
          width={250}
          options={options}
        />
      </div>
    );
  }
}
